require "jekyll_downloadbutton/version"

module Jekyll
  # {% download filename1.pdf:text1,filename.pdf:text2 %}
  class DownloadbuttonTag < Liquid::Tag
    def initialize(tag_name, text, tokens)
      super
      @text     = text.to_s.strip
    end

    def render(context)
      site = context.registers[:site]

      if defined? site.config["cdnurl"]
        baseurl = site.config["cdnurl"]
      else
        baseurl = ""
      end

      toOutput = '<div class="buttons">'

      @text.split("\n").each do |element|
        filename = element.split(":")[0].strip
        text = element.split(":")[1].strip

        file_src = "#{baseurl}/assets/files/#{filename}"

        toOutput += "<a download class=\"matomo_download button is-link\" href=\"#{file_src}\">"
        toOutput +=   '<span class="icon is-small">'
        toOutput +=     '<i class="icon download"></i>'
        toOutput +=   '</span>'
        toOutput +=   '<span>'
        toOutput +=     text
        toOutput +=   '</span>'
        toOutput += '</a>'
      end

      toOutput += '</div>'

      toOutput.concat(" ")
    end

  end
end

Liquid::Template.register_tag('download', Jekyll::DownloadbuttonTag)