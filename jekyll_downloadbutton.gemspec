# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jekyll_downloadbutton/version'

Gem::Specification.new do |spec|
  spec.name          = "jekyll_downloadbutton"
  spec.version       = JekyllDownloadbutton::VERSION
  spec.authors       = ["Sebastian Elisa Pfeifer"]
  spec.email         = ["sebastian@unicorncloud.org"]
  spec.summary       = %q{Add a tag to generate download buttons in Jekyll}
  spec.homepage      = "https://gitlab.com/deadda7a/jekyll-downloadbutton"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake"
end